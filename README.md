# BWSIC

A fantasy microcomputer that runs on WASM and implements a
dialect of BASIC.

_BWSIC_ might stand for BASIC Web System Internet Code.

Vague idea: a 1980s style microcomputer (think C64) running
BASIC. All implemented in WASM.


## Screen handling via TTY

The JavaScript enclosure provides a TTY-like serial interface
(possibly using unicode codepoints, like my Hello World, rather
than bytes).
The BASIC runtime can then update the screen by treating it as
an ANSI terminal.


## Screen update via memory

(I think this is more amusing and promising)
Map a memory block onto the visual display,
much like the VIC-II chip.
In this model the BASIC runtime could update the display by
POKEing (or the equivalent of).

It may still be sensible to have a TTY-like interface in such a
model, but now the interface would be internal to the WASM
program.


## TTY or Memory?

For Memory:

- more in the spirit of microcomputer
- likely to be other "system things" we can POKE
- more straightforward graphics modes?

For TTY:

- moves the implementation into JavaScript
- simpler for BASIC runtime (same as above)


## Sample memory map:

    POKE ADDR,V

In general, it would be amusing to not place any particular
restrictions on _V_.
For example it could a wider (32-, 64-bit) int, a float. A
string?

    $FFF000 System start
    $FFFFFF Systerm end
    $000000 Screen start
    $0003E8 Screen end (Text Mode)

When ADDR is < 0, wrap around.
So negative addresses are for system variables,
positive variables are for screen.


## Sample screen cell memory

Like C64 some video modes will be text based:
the screen will comprise an rectangular matrix of character
cells.
Probably not just limited to 40×25 and possibly quite
programmable?

For unicode and other purposes there is some convenience to
making each cell 32-bits:

    31        21 20                   0
    +-----------+----------------------+
    | Mischief  |   Unicode codepoint  |
    +-----------+----------------------+

"Mischief" is 11 bits not yet allocated for things like
foreground and background colour, flashing, bold, underline,
secret, and so on.


## Tokenisation

BWSIC programs should be stored in traditional tokenised format.
Potentially this is of use to the host as well, allowing
for the possibility that the host might tokenise and transmit
BWSIC programs to the BWSIC runtime system.

In the Unicode era, it's obvious that the basic unit should be
Unicode character stored as its numeric codepoint.
In a 32-bit quantity.

_Tokens_ are lexical-level entities that
have been processed from their raw character sequence form, and
are generally some sort of "special" thing in BWSIC:

- BWSIC keyword
- literal numbers, integer and float
- (possibly) introduction of string literal
- other things

Traditionally in BASIC, tokens are single-byte values outside of
the ASCII range (that is > 127).
In BWSIC we can use values outside of the Unicode range (>
0x10FFFF) and/or values in the Unicode Private Use Areas (PUA).
The Unicode PUAs come in three ranges: 6400 in one (BMP) and
65534 in the other two (planes 15 and 16).
For entirely internal purposes, and i think tokenised BWSIC
programs would count, it would be acceptable to use the 4
non-characters in planes 15 and 16 and therefore have a
contiguous 17-bit range if desired.

As a sketch example, a 32-bit literal integer could be encoded
in two 16-bit segments, using one `u32` for each segment.

In a `u32` container, the non-Unicode range is > 31 bits, which
is huge from a tokenisation persective.

One possible encoding could be fairly uniform across all types:
a format-length `u32` word, followed by `n` words.

- 32-bit integers take 2 words (format-length + 1 additional word)
- 64-bit integers take 3 words
- v128 integers take 5 words
- 32-bit floats take 2 words
- 64-bit floats take 4 words
- strings: 1 words + their length

With a variable-length type field, string length could be
encoded up to 30 bits, which would be the maximum string length
in a 32-bit memory anyway.

Sketch:

- 00nnnnnn N N N  string, 30 bits of length
- 01010000 N N N  integer, 24 bits of length
- 01110000 N N N  float, 24 bits of length


# END
