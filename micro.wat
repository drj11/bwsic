(module
  (memory (import "js" "mem") 1)
  (func $p (import "$" "print") (param i32))
  (func $c (import "$" "putc") (param i32))

  (func (export "hello")
    i32.const 0x2f      ;; ASCII /
    call $c
    i32.const 0x2f      ;; ASCII /
    call $c

    i32.const 0
    i32.const 0x53      ;; ASCII S
    i32.store
    i32.const 4
    i32.const 0x79      ;; ASCII y
    i32.store
    i32.const 8
    i32.const 0x73      ;; ASCII s
    i32.store
    i32.const 0x00c
    i32.const 0x74      ;; ASCII t
    i32.store
    i32.const 0x010
    i32.const 0x65      ;; ASCII e
    i32.store
    i32.const 0x014
    i32.const 0x6d      ;; ASCII m
    i32.store
  )
)
